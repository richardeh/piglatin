"""
PigLatin.py
Reads in a word or string and converts it to Pig Latin

Author: Richard Harrington
Created: 9/4/2013
Last updated: 9/4/2013
"""

def first_vowel(s):
# Finds the first vowel in a word
    for c in s:
        if c.lower() in vowels:
            return s.index(c)

vowels=["a","e","i","o","u"]

p=[]
s=input("Please input a string to convert to Pig Latin:\n")

s=str.split(s)

for word in s:
    if word[0].lower() not in vowels:
        p.append(word[first_vowel(word):len(word)]+word[0:first_vowel(word)]+"ay")
    else:
        p.append(word+"way")
print(" ".join(p))
